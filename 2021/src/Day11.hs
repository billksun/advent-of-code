{-# LANGUAGE OverloadedStrings #-}

module Day11
  ( solve,
  )
where

import Data.List ((\\))
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe (fromJust)
import qualified Data.Text as T
import Data.Void
import qualified Debug.Trace as Debug
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser (Map Coord Int)
inputParser = do
  input <- many lineParser
  many newline
  return $ toPositionMap input
  where
    toPositionMap = Map.fromList . concat . zipWith goRows [1 ..]
      where
        goRows y = zipWith (goCols y) [1 ..]
        goCols y x energyLevel = ((x, y), energyLevel)

lineParser :: Parser [Int]
lineParser = do
  line <- some digitChar <* newline
  return $ read . return <$> line

parseInput :: String -> Map Coord Int
parseInput s = case parse inputParser "Day 9 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

type Coord = (Int, Int)

solve = solution2 . parseInput

solution1 :: Map Coord Int -> Int
solution1 = sum . fmap (length . Map.filter (== 0)) . doSteps [1 .. 100]

solution2 :: Map Coord Int -> Int
solution2 = length . takeWhile (\octopuses -> length octopuses /= length (Map.filter (== 0) octopuses)) . doSteps [1 ..]

doSteps :: [Int] -> Map Coord Int -> [Map Coord Int]
doSteps times octopuses = scanl (\octs _ -> step octs) octopuses times

step :: Map Coord Int -> Map Coord Int
step = resetFlashedOctopuses . flashAndChargeUpNeighbors . chargeAllEnergy
  where
    chargeAllEnergy = fmap succ
    flashAndChargeUpNeighbors octopuses = flashAndChargeUpNeighbors' (getChargedUpOctopuses octopuses) [] octopuses
      where
        getChargedUpOctopuses = Map.assocs . Map.filter (> 9)
        flashAndChargeUpNeighbors' :: [(Coord, Int)] -> [Coord] -> Map Coord Int -> Map Coord Int
        flashAndChargeUpNeighbors' [] _ octopuses = octopuses
        flashAndChargeUpNeighbors' ((coord, _) : _) flashedOctopuses octopuses =
          flashAndChargeUpNeighbors' (filter nonFlashedOctopuses (getChargedUpOctopuses updatedOctopuses)) updatedFlashedOctopuses updatedOctopuses
          where
            neighbors = getNeighbors octopuses coord
            chargeUpNeighbors coords octopuses = foldr (Map.adjust succ) octopuses coords
            updatedOctopuses = chargeUpNeighbors neighbors octopuses
            updatedFlashedOctopuses = coord : flashedOctopuses
            nonFlashedOctopuses (coord, _) = coord `notElem` updatedFlashedOctopuses
    resetFlashedOctopuses :: Map Coord Int -> Map Coord Int
    resetFlashedOctopuses = fmap resetEnergy
      where
        resetEnergy energy
          | energy > 9 = 0
          | otherwise = energy

getNeighbors :: Map Coord Int -> Coord -> [Coord]
getNeighbors locMap (x, y) =
  filter
    validPositions
    [ (x + 1, y),
      (x -1, y),
      (x, y + 1),
      (x, y -1),
      (x + 1, y + 1),
      (x + 1, y - 1),
      (x - 1, y + 1),
      (x - 1, y - 1)
    ]
  where
    validPositions (x, y) = x >= 1 && x <= maxRow && y >= 1 && y <= maxCol
    (maxRow, maxCol) = fst $ fromJust $ Map.lookupMax locMap
