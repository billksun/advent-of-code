{-# LANGUAGE OverloadedStrings #-}

module Day12
  ( solve,
  )
where

import Data.Char (isUpper)
import Data.Functor (($>))
import Data.List (group, sort)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import Data.Void (Void)
import qualified Debug.Trace as Debug
import Text.Megaparsec
  ( Parsec,
    errorBundlePretty,
    many,
    parse,
    (<|>),
  )
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser Graph
inputParser = do
  vertexPairs <- many vertexPairParser
  return (toGraph vertexPairs)
  where
    toGraph :: [(Vertex, Vertex)] -> Graph
    toGraph ps = Map.fromListWith (<>) (foldr prepVertices [] ps)
      where
        prepVertices (first, second) xs = (first, [second]) : (second, [first]) : xs

vertexPairParser :: Parser (Vertex, Vertex)
vertexPairParser = do
  left <- vertexParser
  char '-'
  right <- vertexParser
  space
  return (left, right)

vertexParser :: Parser Vertex
vertexParser = start <|> end <|> cave
  where
    start = string "start" $> Start
    end = string "end" $> End
    cave = do
      name <- many letterChar
      return (caveVertex name)
      where
        caveVertex name
          | all isUpper name = Big name
          | otherwise = Small name

parseInput :: String -> Graph
parseInput s = case parse inputParser "Day 9 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

type Graph = Map Vertex [Vertex]

data Vertex = Start | Small String | Big String | End
  deriving (Ord, Eq, Show)

solve = solution2 . parseInput

solution1 :: Graph -> Int
solution1 = getPathCount [Start]

solution2 :: Graph -> Int
solution2 = getPathCount' [Start]

getPathCount :: [Vertex] -> Graph -> Int
getPathCount [] _ = 0
getPathCount (End : _) _ = 1
getPathCount path@(vertex : _) caves =
  sum $ map (\cave -> getPathCount (cave : path) caves) possibleNextSteps
  where
    possibleNextSteps = filter isValidNextStep $ caves Map.! vertex
    isValidNextStep (Big _) = True
    isValidNextStep cave = cave `notElem` path

getPathCount' :: [Vertex] -> Graph -> Int
getPathCount' [] _ = 0
getPathCount' (End : _) _ = 1
getPathCount' path@(vertex : _) caves =
  sum $ map (\cave -> getPathCount' (cave : path) caves) possibleNextSteps
  where
    possibleNextSteps = filter isValidNextStep $ caves Map.! vertex
    isValidNextStep (Big _) = True
    isValidNextStep smallCave@(Small _) = 
      case canStillVisitSmallCave path of
        True -> True
        _ -> smallCave `notElem` path
    isValidNextStep cave = cave `notElem` path
    canStillVisitSmallCave = (1 ==) . product . fmap length . group . sort . findSmallCaves
    findSmallCaves = filter isSmallCave
    isSmallCave (Small _) = True
    isSmallCave _ = False
