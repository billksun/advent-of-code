module Main where

import qualified Day01
import qualified Day02
import qualified Day03
import qualified Day04
import qualified Day05
import qualified Day06
import qualified Day07
import qualified Day08
import qualified Day09
import qualified Day10
import qualified Day11
import qualified Day12
import qualified Day13
import qualified Day14
import qualified Day15
 
main :: IO ()
main = do
  putStr "Day 01: "
  readFile "data/day01-input" >>= toInts >>= print . Day01.solve
  putStr "Day 02: "
  readFile "data/day02-input" >>= toPairs >>= print . Day02.solve
  putStr "Day 03: "
  readFile "data/day03-input" >>= toBits >>= print . Day03.solve
  putStr "Day 04: "
  readFile "data/day04-input" >>= print . Day04.solve
  putStr "Day 05: "
  readFile "data/day05-input" >>= print . Day05.solve
  putStr "Day 06: "
  readFile "data/day06-input" >>= print . Day06.solve
  putStr "Day 07: "
  readFile "data/day07-input" >>= print . Day07.solve
  putStr "Day 08: "
  readFile "data/day08-input" >>= print . Day08.solve
  putStr "Day 09: "
  readFile "data/day09-input" >>= print . Day09.solve
  putStr "Day 10: "
  readFile "data/day10-input" >>= print . Day10.solve
  putStr "Day 11: "
  readFile "data/day11-input" >>= print . Day11.solve
  putStr "Day 12: "
  readFile "data/day12-input" >>= print . Day12.solve
  putStr "Day 13: "
  print ""
  readFile "data/day13-input" >>= putStr . Day13.solve
  putStr "Day 14: "
  readFile "data/day14-input" >>= print . Day14.solve
  putStr "Day 15: "
  readFile "data/day15-input" >>= print . Day15.solve

toInts :: String -> IO [Int]
toInts x = return $ fmap read $ lines x

toPairs :: String -> IO [(String, Int)]
toPairs x = return $ fmap toPair $ fmap words $ lines x
  where
    toPair :: [String] -> (String, Int)
    toPair y = (y !! 0, read (y !! 1))

toBits :: String -> IO [[Day03.Bit]]
toBits x = return $ fmap (fmap Day03.toBit) $ lines x
