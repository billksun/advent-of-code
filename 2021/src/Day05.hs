{-# LANGUAGE OverloadedStrings #-}

module Day05
  ( solve,
    test
  )
where

import Data.List (deleteBy, group, sort)
import qualified Data.Text as T
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

type Point = (Int, Int)

data Line = Line
  { start :: Point,
    end :: Point
  }
  deriving (Show, Eq)

type Parser = Parsec Void T.Text

inputParser :: Parser [Line]
inputParser = many lineParser

lineParser :: Parser Line
lineParser = do
  x1 <- L.decimal
  char ','
  y1 <- L.decimal
  string " -> "
  x2 <- L.decimal
  char ','
  y2 <- L.decimal <* space1
  return $ Line (x1, y1) (x2, y2)

parseInput :: String -> [Line]
parseInput s = case parse inputParser "Day 5 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

test = solve

solve :: String -> Int
solve = solution1

solution1 :: String -> Int
solution1 = countOverlappingPoints . overlappingPoints . foldMap lineToPoints . parseInput

solution2 :: String -> Int
solution2 = countOverlappingPoints . overlappingPoints . foldMap lineToPoints' . parseInput

countOverlappingPoints :: [[Point]] -> Int
countOverlappingPoints = foldr go 0
  where
    go [p] acc = acc
    go _ acc = acc + 1

overlappingPoints :: [Point] -> [[Point]]
overlappingPoints = group . sort

lineToPoints :: Line -> [Point]
lineToPoints (Line (x1, y1) (x2, y2))
  | x1 == x2 = [(x1, yn) | yn <- [min y1 y2 .. max y1 y2]]
  | y1 == y2 = [(xn, y1) | xn <- [min x1 x2 .. max x1 x2]]
  | otherwise = []

lineToPoints' :: Line -> [Point]
lineToPoints' (Line (x1, y1) (x2, y2))
  | x1 == x2 = [(x1, yn) | yn <- [min y1 y2 .. max y1 y2]]
  | y1 == y2 = [(xn, y1) | xn <- [min x1 x2 .. max x1 x2]]
  | abs (x1 - x2) == abs (y1 - y2) = [(xn, yn) | xn <- [min x1 x2 .. max x1 x2], yn <- [min y1 y2 .. max y1 y2], abs (xn - x1) == abs (yn - y1)]
  | otherwise = []
