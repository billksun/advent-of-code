module Day03
  ( solve,
    toBit,
    Bit,
  )
where

import Data.List (foldl', scanl1, transpose)

solve :: [[Bit]] -> Int
solve = solution2

solution1 :: [[Bit]] -> Int
solution1 input = gamma * epsilon
  where
    gamma = toNumber gammaBits
    gammaBits = mostCommonBit <$> transpose input
    epsilon = toNumber epsilonBits
    epsilonBits = fmap flipBit gammaBits

solution2 :: [[Bit]] -> Int
solution2 input = toNumber (ratingO2 input 0) * toNumber (ratingCO2 input 0)
  where
    ratingO2 :: [[Bit]] -> Int -> [Bit]
    ratingO2 [x] _ = x
    ratingO2 bits idx = ratingO2 (filter ((== mostCommonBit (transpose bits !! idx)) . (!! idx)) bits) (idx + 1)
    ratingCO2 :: [[Bit]] -> Int -> [Bit]
    ratingCO2 [x] _ = x
    ratingCO2 bits idx = ratingCO2 (filter ((== leastCommonBit (transpose bits !! idx)) . (!! idx)) bits) (idx + 1)

data Bit = Zero | One deriving (Eq, Ord, Show, Enum)

toBit :: Char -> Bit
toBit '0' = Zero
toBit _ = One

flipBit :: Bit -> Bit
flipBit Zero = One
flipBit One = Zero

data Count = Count
  { ones :: Int,
    zeroes :: Int
  }

instance Semigroup Count where
  Count a b <> Count c d = Count (a + c) (b + d)

instance Monoid Count where
  mempty = Count 0 0

mostCommonBit :: [Bit] -> Bit
mostCommonBit bits =
  if ones count >= zeroes count
    then One
    else Zero
  where
    count = bitCount bits

leastCommonBit :: [Bit] -> Bit
leastCommonBit = flipBit . mostCommonBit

bitCount :: [Bit] -> Count
bitCount bits = last $ dropWhile (\x -> ones x > midpoint || zeroes x > midpoint) $ scanl1 (<>) $ map toCount bits
  where
    midpoint = (length bits) `div` 2 + 1
    toCount Zero = Count 0 1
    toCount One = Count 1 0

toNumber :: [Bit] -> Int
toNumber = foldl' convert 0
  where
    convert acc x = 2 * acc + fromEnum x -- using the double dabble algorithm
