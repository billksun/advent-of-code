{-# LANGUAGE OverloadedStrings #-}

module Day06
  ( solve,
  )
where

import Data.Sequence (Seq (..), (<|), (|>))
import qualified Data.Sequence as Seq (adjust', empty, foldrWithIndex, replicate, update)
import qualified Data.Text as T
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser [Int]
inputParser = do
  L.decimal `sepBy` char ','

parseInput :: String -> [Int]
parseInput s = case parse inputParser "Day 6 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

-- solve :: String -> Int
solve = solution2

solution1 = length . fishPopulation (take 80 days) . parseInput

solution2 = foldr1 (+) . fishPopulationByBuckets (take 256 days) . parseInput

days = [1 ..]

fishPopulation :: [Int] -> [Int] -> [Int]
fishPopulation days seedPopulation = foldr simulate seedPopulation days
  where
    simulate :: Int -> [Int] -> [Int]
    simulate _ fishes = foldr updateCounter [] fishes

fishPopulationByBuckets :: [Int] -> [Int] -> Seq Int
fishPopulationByBuckets days seedPopulation = foldr updateBuckets (initializeBuckets ageBuckets seedPopulation) days

updateCounter :: Int -> [Int] -> [Int]
updateCounter 0 fishes = 6 : 8 : fishes
updateCounter t fishes = (t - 1) : fishes

ageBuckets :: Seq Int
ageBuckets = Seq.replicate 9 0

initializeBuckets :: Seq Int -> [Int] -> Seq Int
initializeBuckets buckets = foldr assignBucket ageBuckets
  where
    assignBucket fishCounter buckets = Seq.adjust' (+ 1) fishCounter buckets

updateBuckets :: Int -> Seq Int -> Seq Int
updateBuckets _ Empty = Seq.empty
updateBuckets _ (x :<| xs) = Seq.adjust' (+ x) 6 $ xs |> x
