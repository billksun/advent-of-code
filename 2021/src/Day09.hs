{-# LANGUAGE OverloadedStrings #-}

module Day09
  ( solve,
  )
where

import Data.List (sortBy, transpose, (\\), union)
import Data.Maybe (fromJust)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Text as T
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser [[Int]]
inputParser = do
  input <- many lineParser
  many newline
  return input

lineParser :: Parser [Int]
lineParser = do
  line <- some digitChar <* newline
  return $ read . return <$> line

parseInput :: String -> [[Int]]
parseInput s = case parse inputParser "Day 9 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

data Location = Location
  { locationHeight :: Int,
    locationHMinimum :: Bool,
    locationVMinimum :: Bool,
    locationCoord :: Maybe Coord,
    locationVisited :: Bool
  }
  deriving (Show, Eq)

type Coord = (Int, Int)

solve s = solution2 input
  where 
    input = parseInput s

solution1 :: [[Int]] -> Int
solution1 = calculateRiskLevel . findMinimums . markVerticalMinimums . markHorizontalMinimums . toLocationMatrix

solution2 :: [[Int]] -> Int
solution2 input = product $ take 3 $ sortBy (flip compare) $ fmap length $ findBasins (minimums input) $ toLocationMap input

minimums :: [[Int]] -> [Location]
minimums = findMinimums . markVerticalMinimums . markHorizontalMinimums . toLocationMatrix

toLocationMatrix :: [[Int]] -> [[Location]]
toLocationMatrix = zipWith goRows [1 ..]
  where
    goRows y = zipWith (goCols y) [1 ..]
    goCols y x height = Location height False False (Just (x, y)) False

markHorizontalMinimums :: [[Location]] -> [[Location]]
markHorizontalMinimums = fmap go
  where
    go = markMinimums (\l b -> l {locationHMinimum = b})

markVerticalMinimums :: [[Location]] -> [[Location]]
markVerticalMinimums = fmap go . transpose
  where
    go = markMinimums (\l b -> l {locationVMinimum = b})

markMinimums :: (Location -> Bool -> Location) -> [Location] -> [Location]
markMinimums f = fmap go . zipWithNextAndPrevious
  where
    zipWithNextAndPrevious locations = zip3 (edgeFiller : init locations) locations (tail locations ++ [edgeFiller])
    edgeFiller = Location 9 False False Nothing False
    go (previous, current, next) = f current $ (locationHeight previous) > (locationHeight current) && (locationHeight current) < (locationHeight next)

findMinimums :: [[Location]] -> [Location]
findMinimums = filter (\l -> locationHMinimum l && locationVMinimum l) . concat

calculateRiskLevel :: [Location] -> Int
calculateRiskLevel = foldr (\l rlevel -> locationHeight l + 1 + rlevel) 0

toLocationMap :: [[Int]] -> Map Coord Location
toLocationMap = Map.fromList . concat . zipWith goRows [1 ..]
  where
    goRows y = zipWith (goCols y) [1 ..]
    goCols y x height = ((x, y), Location height False False (Just (x, y)) False)

findBasins :: [Location] -> Map Coord Location -> [[Location]]
findBasins minimums locMap = fmap (expandBasin locMap) minimums 

expandBasin :: Map Coord Location -> Location -> [Location]
expandBasin locMap start = go [] locMap [start]
  where
    go :: [Location] -> Map Coord Location -> [Location] -> [Location]
    go basin locMap [] = basin
    go basin locMap (loc:locs) = 
      if locationHeight loc == 9
        then go basin updatedLocMap locs
        else go (loc':basin) updatedLocMap (addNewNeighborsToQueue locs)
      where
        loc' = loc { locationVisited = True }
        updatedLocMap = markCurrentLocationAsVisited locMap
        visitedLocations = Map.elems . Map.filter (\l -> locationVisited l == True)
        markCurrentLocationAsVisited = Map.adjust (\_ -> loc') currentLocationCoord
        currentLocationCoord = fromJust $ locationCoord loc'
        addNewNeighborsToQueue queue = ((Map.elems $ Map.restrictKeys updatedLocMap (getNeighbors updatedLocMap currentLocationCoord)) \\ (visitedLocations updatedLocMap)) `union` locs
 
    getNeighbors :: Map Coord Location -> Coord -> Set Coord
    getNeighbors locMap (x, y) = Set.fromList $ filter validLocations [(x + 1, y), (x -1, y), (x, y + 1), (x, y -1)]
      where 
        validLocations (x, y) = x >= 1 && x <= maxRow && y >= 1 && y <= maxCol
        (maxRow, maxCol) = fst $ fromJust $ Map.lookupMax locMap

