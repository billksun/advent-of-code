{-# LANGUAGE OverloadedStrings #-}

module Day08
  ( solve,
  )
where

import Data.List (union, find, foldl', sort)
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser [([String], [String])]
inputParser = do
  many entryParser

entryParser :: Parser ([String], [String])
entryParser = do
  patterns <- signalPatternsParser
  char '|'
  output <- displayOutputParser 
  newline
  return (patterns, output)
  
signalPatternsParser :: Parser [String]
signalPatternsParser = do
  count 10 $ some lowerChar <* space1

displayOutputParser :: Parser [String]
displayOutputParser = do
  count 4 $ space1 *> some lowerChar

parseInput :: String -> [([String], [String])]
parseInput s = case parse inputParser "Day 8 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

solve = solution2. parseInput

solution1 :: [([String], [String])] -> Int
solution1 = countPartialOutputs

solution2 :: [([String], [String])] -> Int
solution2 = sum . fmap go
  where
    go (signals, output) = decodeOutput output (mkSignalToDigitMap signals)

countPartialOutputs :: [([String], [String])] -> Int
countPartialOutputs = foldr countPartialOutput 0 
  where
    countPartialOutput (_, output) count = count + length (filter (`elem` [2, 4, 3, 7]) (fmap length output))

decodeOutput :: [String] -> [(String, Int)] -> Int
decodeOutput output signalMap = toNumber $ fmap go output
  where
    go :: String -> Int
    go signal = case find (\s -> sort signal == sort (fst s)) signalMap of
      Just m -> snd m
      Nothing -> error ("Unknown signal detected when decoding: " <> signal)

    toNumber :: [Int] -> Int
    toNumber = read . foldl' (\s d -> s <> show d) ""

mkSignalToDigitMap :: [String] -> [(String, Int)]
mkSignalToDigitMap signals = simpleSignals <> complexSignals
  where
    (simpleSignals, unprocessedSignals) = processSimpleSignals signals
    complexSignals = processComplexSignals simpleSignals unprocessedSignals
    
processSimpleSignals :: [String] -> ([(String, Int)], [String])
processSimpleSignals = foldr go ([], [])
  where
    go signal (knowns, unknowns)
      | length signal == 2 = addToKnowns signal 1
      | length signal == 3 = addToKnowns signal 7
      | length signal == 4 = addToKnowns signal 4
      | length signal == 7 = addToKnowns signal 8
      | otherwise = addToUnknowns signal
      where
        addToKnowns signal digit = ((signal, digit) : knowns, unknowns)
        addToUnknowns signal = (knowns, signal : unknowns)

processComplexSignals :: [(String, Int)] -> [String] -> [(String, Int)]
processComplexSignals simples = foldr go []
  where
    go signal complexes
      | length signal == 5 = processLength5s signal : complexes
      | length signal == 6 = processLength6s signal : complexes
      | otherwise = complexes

    processLength5s :: String -> (String, Int)
    processLength5s s
      | all (`elem` s) (findSignalFor 1) = (s, 3)
      | all (`elem` (s `union` findSignalFor 4)) (findSignalFor 8) = (s, 2)
      | otherwise = (s, 5)

    processLength6s :: String -> (String, Int)
    processLength6s s
      | not $ all (`elem` s) (findSignalFor 1) = (s, 6)
      | all (`elem` s) (findSignalFor 4) = (s, 9)
      | otherwise = (s, 0)

    findSignalFor d =
      case find (\p -> d == snd p) simples of
        Nothing -> error $ "Signal for " <> show d <> " not found!" <> "in : " <> show simples
        Just (s, _) -> s
