{-# LANGUAGE OverloadedStrings #-}

module Day07
  ( solve,
  )
where

import qualified Data.Text as T
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser [Int]
inputParser = do
  L.decimal `sepBy` char ','

parseInput :: String -> [Int]
parseInput s = case parse inputParser "Day 7 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

solve = solution2 . parseInput

solution1 :: [Int] -> Int
solution1 = findMinFuelCost calculateFuelFor

solution2 :: [Int] -> Int
solution2 = findMinFuelCost calculateFuelFor'

findMinFuelCost :: ([Int] -> Int -> Int) -> [Int] -> Int
findMinFuelCost costFunc ps = minimum $ fmap (costFunc ps) [minimum ps .. maximum ps]

calculateFuelFor :: [Int] -> Int -> Int
calculateFuelFor ps p = sum $ fmap (abs . (p -)) ps

calculateFuelFor' :: [Int] -> Int -> Int
calculateFuelFor' ps p = sum $ fmap (fuelUsed . abs . (p -)) ps
  where
    fuelUsed steps = sum [1 .. steps]
