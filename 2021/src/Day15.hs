{-# LANGUAGE OverloadedStrings #-}

module Day15
  ( solve,
  )
where

{- ORMOLU_DISABLE -}
import qualified Data.Text as T
import Data.Void
import qualified Debug.Trace as Debug
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
{- ORMOLU_ENABLE -}

import Algorithm.Search (dijkstra)
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.HashSet (HashSet)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import qualified Data.HashSet as HashSet
import Data.List (sort, transpose)
import Data.Maybe (fromJust, fromMaybe)

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser Input
inputParser = some lineParser

lineParser :: Parser [Int]
lineParser = (fmap (read . pure) <$> some digitChar) <* space1

parseInput :: String -> Input
parseInput s = case parse inputParser "Day 9 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

solve = pickBottomRight . findPaths . toRiskMap . mkFullInput . parseInput

-- solution1 :: (String, Rules) -> Int
solution1 = pickBottomRight . findPaths . toRiskMap

solution2 :: Input  -> Int
solution2 input =
  maybe 0 fst $
  dijkstra
    (getNeighbors maxRowCol)
    cost
    end
    start
  where
    riskMap = toRiskMap $ mkFullInput input
    cost _ s' = fromMaybe 999 $ Map.lookup s' riskMap
    end = (== maxRowCol)
    start = (0,0)
    maxRowCol = getMaxRowCol riskMap

type Input = [[Int]]

type RiskMap = Map Coord Int

type Queue = (Map Coord (Coord, Int), HashSet Coord)

type LowestRiskPath = Map Coord (Int, Maybe Coord)

type Coord = (Int, Int)

toRiskMap :: Input -> RiskMap
toRiskMap = Map.fromList . foldMap (zipWith zipCols [0 ..]) . transpose . fmap (zip [0 ..])
  where
    zipCols :: Int -> (Int, Int) -> (Coord, Int)
    zipCols y (x, riskValue) = ((x, y), riskValue)

findPaths :: RiskMap -> LowestRiskPath
findPaths riskMap = dijkstra' maxRowCol (initialUnvisited, mempty) (Map.insert (0, 0) (0, Nothing) mempty) riskMap
  where
    maxRowCol = getMaxRowCol riskMap

dijkstra' :: (Int, Int) -> Queue -> LowestRiskPath -> RiskMap -> LowestRiskPath
dijkstra' maxRowCol q@(unvisited, visited) pathMap riskMap
  | unvisited == mempty = pathMap
  | Map.member maxRowCol pathMap = pathMap
  | otherwise =
    dijkstra' maxRowCol q' pathMap' riskMap
  where
    (q', pathMap') = Map.foldrWithKey go ((unvisited', visited'), pathMap) unvisitedNeighbors
      where
        go :: Coord -> Int -> (Queue, LowestRiskPath) -> (Queue, LowestRiskPath)
        go coord riskValue ((newUnvisited, newVisited), newPathMap) =
          case existingPath of
            Nothing -> ((addToQueue pathRisk coord newUnvisited, newVisited), Map.insert coord (pathRisk, Just (snd currentLocation)) newPathMap)
            Just (existingPathRisk, _) ->
              if pathRisk < existingPathRisk
                then ((addToQueue pathRisk coord newUnvisited, newVisited), Map.insert coord (pathRisk, Just (snd currentLocation)) newPathMap)
                else ((newUnvisited, newVisited), newPathMap)
          where
            pathRisk = riskValue + fst currentLocation
            existingPath = Map.lookup coord pathMap
            addToQueue r c = Map.insert c (c, r)
    (currentLocation, (unvisited', visited')) = fromJust $ removeMin q
    unvisitedNeighbors = Map.filterWithKey (\k _ -> k `elem` unvisitedNeighborCoords) riskMap
      where
        unvisitedNeighborCoords = filter (\coord -> not $ HashSet.member coord visited') $ getNeighbors maxRowCol (snd currentLocation)

removeMin :: Queue -> Maybe ((Int, Coord), Queue)
removeMin (unvisited, visited) =
  case min of
    Nothing -> Nothing
    Just m -> Just (m, (Map.delete (snd m) unvisited, HashSet.insert (snd m) visited))
  where
    min = findMin unvisited
    findMin :: Map Coord (Coord, Int) -> Maybe (Int, Coord)
    findMin = foldr go Nothing
      where
        go :: (Coord, Int) -> Maybe (Int, Coord) -> Maybe (Int, Coord)
        go (coord, pathRisk) Nothing = Just (pathRisk, coord)
        go (coord1, pathRisk1) (Just (pathRisk2, coord2)) =
          if pathRisk1 < pathRisk2
            then Just (pathRisk1, coord1)
            else Just (pathRisk2, coord2)

getNeighbors :: (Int, Int) -> Coord -> [Coord]
getNeighbors (maxRow, maxCol) (x, y) =
  filter
    validPositions
    [ (x + 1, y),
      (x -1, y),
      (x, y + 1),
      (x, y -1)
    ]
  where
    validPositions (x, y) = x >= 0 && x <= maxRow && y >= 0 && y <= maxCol

mkFullInput :: Input -> Input
mkFullInput = concat . take 5 . iterate (fmap increase) . fmap (concat . take 5 . iterate increase)
  where
    increase = fmap (\x -> (x `mod` 9) + 1)

pickBottomRight :: LowestRiskPath -> Maybe (Int, Maybe Coord)
pickBottomRight pathMap = Map.lookup (maxRow, maxCol) pathMap
  where
    (maxRow, maxCol) = getMaxRowCol pathMap

getMaxRowCol :: Map Coord a -> Coord
getMaxRowCol = maximum . Map.keys

initialUnvisited :: Map Coord (Coord, Int)
initialUnvisited = Map.insert (0, 0) ((0, 0), 0) mempty
