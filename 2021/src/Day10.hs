{-# LANGUAGE OverloadedStrings #-}

module Day10
  ( solve,
  )
where

import qualified Data.Text as T
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import Data.List (sort, foldl')
import Data.Maybe (mapMaybe)

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser [String]
inputParser = 
  many lineParser

lineParser :: Parser String
lineParser = 
  some (satisfy isBracket) <* space1
  where
    isBracket :: Char -> Bool
    isBracket = (`elem` ['(', ')', '<', '>', '[', ']', '{', '}'])

parseInput :: String -> [String]
parseInput s = case parse inputParser "Day 9 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

data BracketType = Round | Square | Curly | Angle
  deriving (Eq, Show)

data Direction = Open | Close deriving (Eq, Show)

data Bracket = Bracket Direction BracketType deriving (Eq, Show)

type UnclosedBracketStack = [BracketType]

solve = solution2 . parseInput

solution1 :: [String] -> Int
solution1 = sum . fmap scoreError . mapMaybe (checkLine [] . fmap toBracket) 

solution2 :: [String] -> Int
solution2 = getMedian . sort . fmap scoreCompletion . mapMaybe (completeLine [] . fmap toBracket)

checkLine :: UnclosedBracketStack -> [Bracket] -> Maybe BracketType
-- Reached end of line, no more unclosed brackets, no errors
checkLine [] [] = Nothing
-- Reached end of line, still have unclosed brackets, incomplete
checkLine stack [] = Nothing
checkLine stack ((Bracket Open bracketType) : bs) =
  checkLine (bracketType : stack) bs
checkLine (s: ss) ((Bracket Close bracketType) : bs)
  | s == bracketType = checkLine ss bs
  -- Unmatched closing bracket found
  | otherwise = Just bracketType
-- Still have more brackets left in input but no more unclose brackets left
checkLine [] brackets = Nothing

scoreError :: BracketType -> Int
scoreError Round = 3
scoreError Square = 57
scoreError Curly = 1197
scoreError Angle = 25137

completeLine :: UnclosedBracketStack -> [Bracket] -> Maybe UnclosedBracketStack
-- Reached end of line, no more unclosed brackets, no errors
completeLine [] [] = Nothing
-- Reached end of line, still have unclosed brackets, incomplete
completeLine stack [] = Just stack
completeLine stack ((Bracket Open bracketType) : bs) =
  completeLine (bracketType : stack) bs
completeLine (s: ss) ((Bracket Close bracketType) : bs)
  | s == bracketType = completeLine ss bs
  -- Unmatched closing bracket found
  | otherwise = Nothing
-- Still have more brackets left in input but no more unclose brackets left
completeLine [] brackets = Nothing

scoreCompletion :: UnclosedBracketStack -> Int
scoreCompletion = foldl' computeScore 0
  where
    computeScore scoreTally x = scoreTally * 5 + bracketValue x

bracketValue :: BracketType -> Int
bracketValue Round = 1
bracketValue Square = 2
bracketValue Curly = 3
bracketValue Angle = 4

getMedian :: [Int] -> Int
getMedian list = list !! (length list `div` 2)

toBracket :: Char -> Bracket
toBracket '(' = Bracket Open Round
toBracket '[' = Bracket Open Square
toBracket '{' = Bracket Open Curly
toBracket '<' = Bracket Open Angle
toBracket ')' = Bracket Close Round
toBracket ']' = Bracket Close Square
toBracket '}' = Bracket Close Curly
toBracket '>' = Bracket Close Angle
toBracket _ = error "invalid bracket type"
