module Day02 (solve) where

solve :: [(String, Int)] -> Int
solve = solution2

solution1 :: [(String, Int)] -> Int
solution1 cmds = horizontal * depth
  where
    horizontal = sumUnits $ filter horizontals cmds
    horizontals c = fst c == "forward"
    depth = (sumUnits $ filter downs cmds) - (sumUnits $ filter ups cmds)
    ups c = fst c == "up"
    downs c = fst c == "down"
    sumUnits = foldr (\x acc -> snd x + acc) 0

solution2 :: [(String, Int)] -> Int
solution2 cmds = horizontal * depth
  where
    horizontal = sumUnits $ filter horizontals cmds
    horizontals c = fst c == "forward"
    sumUnits = foldr (\x acc -> snd x + acc) 0
    (depth,aim) = foldr go (0,0) (reverse cmds)
    go ("forward",x) (d,a) = (d+x*a,a)
    go ("up",x) (d,a) = (d,a-x)
    go ("down",x) (d,a) = (d,a+x)
    go _ da = da
