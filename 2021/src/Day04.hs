{-# LANGUAGE OverloadedStrings #-}

module Day04
  ( solve,
    test
  )
where

import Data.Foldable (fold, foldl')
import Data.List (find, inits, partition, takeWhile, transpose)
import Data.Maybe (fromJust)
import qualified Data.Text as T (Text, pack)
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

test = calculateScore . findLastBingo . parseInput

solve :: String -> Int
solve = solution2

solution1 :: String -> Int
solution1 = calculateScore . findFirstBingo . parseInput

solution2 :: String -> Int
solution2 = calculateScore . findLastBingo . parseInput

type Input = ([Int], [Board])

parseInput :: String -> Input
parseInput s = case parse inputParser "Day 4 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

sc :: Parser ()
sc =
  L.space
    space1
    empty
    empty

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: T.Text -> Parser T.Text
symbol = L.symbol sc

integer :: Parser Int
integer = lexeme L.decimal

type Parser = Parsec Void T.Text

inputParser :: Parser Input
inputParser = do
  drawnNumbers <- integer `sepBy` char ','
  boards <- many board
  return (drawnNumbers, fmap Board boards)
  where
    board = count 5 $ count 5 integer

newtype Board = Board [[Int]] deriving (Eq)

instance Show Board where
  show (Board board) = unlines $ fmap show board

findFirstBingo :: Input -> (Board, [Int])
findFirstBingo = head . findBingos

findLastBingo :: Input -> (Board, [Int])
findLastBingo = last . findBingos

findBingos :: Input -> [(Board, [Int])]
findBingos (numbers, boards) = fst $ foldl' go ([], boards) $ tail $ inits numbers
  where
    go :: ([(Board, [Int])], [Board]) -> [Int] -> ([(Board, [Int])], [Board])
    go (winningBoards, boardsInPlay) drawnNumbers = (winningBoards ++ ((\winner -> (winner, drawnNumbers)) <$> winners), losers)
      where
        (winners, losers) = commenceRound drawnNumbers boardsInPlay
    commenceRound drawnNumbers boardsInPlay = partition (isWinner drawnNumbers) boardsInPlay

calculateScore :: (Board, [Int]) -> Int
calculateScore (Board board, drawnNumbers) = sum unmarkedNumbers * last drawnNumbers
  where
    unmarkedNumbers = filter (`notElem` drawnNumbers) $ fold board

isWinner :: [Int] -> Board -> Bool
isWinner drawnNumbers (Board b) =
  any wins b || any wins (transpose b)
  where
    wins = all (`elem` drawnNumbers)
