{-# LANGUAGE OverloadedStrings #-}

module Day13
  ( solve,
  )
where

import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import Data.List (foldl')
import qualified Data.Text as T
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser (Paper, [Fold])
inputParser = do
  points <- many pointParser
  folds <- many foldParser
  return (HashSet.fromList points, folds)

pointParser :: Parser Point
pointParser = do
  x <- L.decimal
  char ','
  y <- L.decimal <* space1
  return (x, y)

foldParser :: Parser Fold
foldParser = yfold <|> xfold
  where
    yfold = do
      string "fold along y="
      UpFold <$> L.decimal <* space1
    xfold = do
      string "fold along x="
      LeftFold <$> L.decimal <* space1

parseInput :: String -> (Paper, [Fold])
parseInput s = case parse inputParser "Day 9 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

solve = solution2 . parseInput

solution1 :: (Paper, [Fold]) -> Int
solution1 (paper, firstFold : folds) = length $ HashSet.map (fold firstFold) paper
solution1 _ = error "Only doing one fold"

solution2 :: (Paper, [Fold]) -> String
solution2 = printPaper . foldPaper

type Paper = HashSet Point

type Point = (Int, Int)

data Fold = LeftFold Int | UpFold Int
  deriving (Eq, Show)

fold :: Fold -> Point -> Point
fold (UpFold n) (x, y) = (x, n - abs (y - n))
fold (LeftFold n) (x, y) = (n - abs (x - n), y)

foldPaper :: (Paper, [Fold]) -> Paper
foldPaper (p, fs) = foldl' go p fs
  where
    go p f = HashSet.map (fold f) p

printPaper :: Paper -> String
printPaper paper = unlines 
  [[if HashSet.member (x, y) paper then '#' else '.' 
    | x <- [0 .. maxX]] 
    | y <- [0 .. maxY]]
  where
    maxX = maximum $ fst <$> HashSet.toList paper
    maxY = maximum $ snd <$> HashSet.toList paper
 