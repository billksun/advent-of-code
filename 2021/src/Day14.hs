{-# LANGUAGE OverloadedStrings #-}

module Day14
  ( solve,
  )
where

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.List (group, sort)
import qualified Data.Text as T
import Data.Void
import qualified Debug.Trace as Debug
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

-------------------------------------------------------------------------------
-- Input parsing
-------------------------------------------------------------------------------

type Parser = Parsec Void T.Text

inputParser :: Parser Input
inputParser = do
  template <- templateParser
  rules <- many ruleParser
  return (template, HashMap.fromList rules)

templateParser :: Parser String
templateParser = many upperChar <* space1

ruleParser :: Parser ((Char, Char), Char)
ruleParser = do
  fstElem <- upperChar
  sndElem <- upperChar
  string " -> "
  insertionElem <- upperChar <* space1
  return ((fstElem, sndElem), insertionElem)

parseInput :: String -> Input
parseInput s = case parse inputParser "Day 9 input file" (T.pack s) of
  Left bundle -> error (errorBundlePretty bundle)
  Right input -> input

-------------------------------------------------------------------------------
-- Solutions
-------------------------------------------------------------------------------

solve = solution1 . parseInput

solution1 :: (String, Rules) -> Int
solution1 (template, rules) = getResult $ applyRulesNTimes 10 rules $ initializeState template

solution2 :: (String, Rules) -> Int
solution2 (template, rules) = getResult $ applyRulesNTimes 40 rules $ initializeState template

type Input = (String, Rules)

type Rules = HashMap (Char, Char) Char

type ElemPairs = HashMap (Char, Char) Int

type ElemTally = HashMap Char Int

type ProgState = (ElemPairs, ElemTally)

getResult :: ProgState-> Int
getResult state = maximum elemGroupCount - minimum elemGroupCount
  where
    elemGroupCount = getElementGroupCounts state
    getElementGroupCounts (_, elemTally) = HashMap.elems elemTally

applyRules :: Rules -> ProgState -> ProgState
applyRules rules (elemPairs, elemTally) = HashMap.foldrWithKey (doApply rules) (HashMap.empty, elemTally) elemPairs
  where
    doApply :: Rules -> (Char, Char) -> Int -> ProgState -> ProgState
    doApply rules pair@(a, b) pairTally (newElemPairs, newElemTally) =
      case HashMap.lookup pair rules of
        Just c -> (incrementPair pairTally (a, c) $ incrementPair pairTally (c, b) newElemPairs, tallyElem pairTally c newElemTally)
        Nothing -> (newElemPairs, newElemTally)

applyRulesNTimes :: Int -> Rules -> ProgState -> ProgState
applyRulesNTimes nTimes rules state = foldr (\_ acc -> applyRules rules acc) state [1 .. nTimes]

toElemPairs :: String -> ElemPairs
toElemPairs template = foldr (incrementPair 1) HashMap.empty $ zip template (tail template)

incrementPair :: Int -> (Char, Char) -> HashMap (Char, Char) Int -> HashMap (Char, Char) Int
incrementPair n pair = HashMap.insertWith (+) pair n 

decrementPair :: Int -> (Char, Char) -> HashMap (Char, Char) Int -> HashMap (Char, Char) Int
decrementPair n = HashMap.adjust (\pairTally -> pairTally - n)

tallyElem :: Int -> Char -> ElemTally -> ElemTally
tallyElem n c = HashMap.insertWith (+) c n

initializeState :: String -> ProgState
initializeState template = (toElemPairs template, initialElemTally template)
  where
    initialElemTally t = foldr (tallyElem 1) HashMap.empty t
