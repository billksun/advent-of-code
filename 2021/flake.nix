{
  description = "A very basic flake";

  inputs = {
    haskellNix.url = "github:input-output-hk/haskell.nix";
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.follows = "haskellNix/nixpkgs-unstable";
    flake-compat = { 
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };
  
  outputs = { self, nixpkgs, flake-utils, flake-compat, haskellNix }:
    flake-utils.lib.eachSystem [ "x86_64-linux" "x86_64-darwin" ] (system:
    let
      package = "aoc";
      overlays = [ haskellNix.overlay
        (final: prev: {
          # This overlay adds our project to pkgs
          helloProject =
            final.haskell-nix.cabalProject' {
              src = ./.;
              compiler-nix-name = "ghc8107";
              modules = [
                { reinstallableLibGhc = true; } # Allow overriding packages pre-installed with GHC with newer version
                { options.nonReinstallablePkgs = 
                    pkgs.lib.mkOption { apply = pkgs.lib.remove "Cabal"; };
                }
              ];
              # This is used by `nix develop .` to open a shell for use with
              # `cabal`, `hlint` and `haskell-language-server`
              shell.tools = {
                cabal = {};
                hlint = {};
                haskell-language-server = {};
                ghcid = {};
                ormolu = { version = "0.3.1.0"; };
              };
              # Non-Haskell shell tools go here
              shell.buildInputs = with pkgs; [
                nixpkgs-fmt
              ];
            };
        })
      ];
      pkgs = import nixpkgs { inherit system overlays; inherit (haskellNix) config; };
      flake = pkgs.helloProject.flake {
        # This adds support for `nix build .#js-unknown-ghcjs-cabal:hello:exe:hello`
        crossPlatforms = p: [p.ghcjs];
      };
    in flake // {
      # Built by `nix build .`
      defaultPackage = flake.packages."${package}:exe:${package}";
    });
}
