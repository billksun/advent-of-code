module Day01 (solve) where

solve :: [Int] -> Int
solve = solution2

solution1 :: [Int] -> Int
solution1 = fst . foldr1 countIncreases . fmap (\x -> (0,x)) . reverse
  where
    countIncreases x acc = 
      if snd x > snd acc
        then (fst acc + 1, snd x)
        else (fst acc, snd x)

solution2 :: [Int] -> Int
solution2 = solution1 . fmap sum . reverse . toWindows []
  where
    toWindows ws [a, b, c] = [a, b, c]:ws 
    toWindows ws (a:b:c:ds) = toWindows ([a, b, c]:ws) (b:c:ds)
    toWindows ws _ = ws
